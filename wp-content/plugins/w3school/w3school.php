<?php
/**
 * Plugin Name: W3scool Plugin
 * Plugin URI: https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_animation2
 * Description: W3school Task Plugin
 * Version: 1.0
 * Author: Akshay Lade
 * Author URI: 
 */

register_activation_hook(__FILE__, 'school_plugin_activate');
register_deactivation_hook(__FILE__, 'school_plugin_deactivate');

function school_plugin_activate() {}


function school_plugin_deactivate() {}


add_action('admin_menu','school_plugin_menu');

function school_plugin_menu(){

    add_menu_page('W3 School', 'W3 School', 3, __FILE__, 'school_plugin_list');
}

function school_plugin_list(){

   include('w3school_template.php');
}