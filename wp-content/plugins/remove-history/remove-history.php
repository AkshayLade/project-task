<?php
/**
 * Plugin Name: Remove History Plugin
 * Plugin URI: https://wordpress.org/plugins/simple-history/
 * Description: Remove User Data From Simple History DB
 * Version: 1.0
 * Author: Akshay Lade
 * Author URI: 
 */

function remove_data($context){

         unset( $context['_user_login'] );
         unset( $context['_user_email'] );
         unset( $context['_server_remote_addr'] );

         return $context;
}

add_filter( 'simple_history/log_insert_context', 'remove_data', 10, 2);